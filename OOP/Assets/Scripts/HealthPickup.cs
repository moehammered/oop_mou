﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour {

    public int recoverAmount;

    private void destroyPickup()
    {
        Destroy(gameObject);
    }

    private void addHealth(Player targetPlayer)
    {
        targetPlayer.playerStats.health += recoverAmount;
    }

    private void OnTriggerEnter(Collider col)
    {
        Player triggeredPlayer = col.gameObject.GetComponent<Player>();
        //!= means not equal to
        if(triggeredPlayer != null) //Did a player component exist?
        {
            //A player was found!!!
            addHealth(triggeredPlayer);
            destroyPickup();
        }
    }
}
