﻿using UnityEngine;
using System.Collections;

[System.Serializable] //MAKES THIS VISIBLE IN THE INSPECTOR!!!!!!
public struct Stats
{
    public int health; //or use float as the type
    public string name;
    public float movementSpeed;
    public float jumpHeight;
    public KeyBinds controls; //Keybinds already exists in the other script!
}

public class Player : MonoBehaviour {

    public Stats playerStats;
    private Rigidbody rigidbody;

    private void checkKeyPresses()
    {
        if( Input.GetKey( playerStats.controls.forwardKey ) )
        {
            moveForward();
        }
        if( Input.GetKey( playerStats.controls.backKey ) )
        {
            moveBack();
        }
        if( Input.GetKey( playerStats.controls.leftKey ) )
        {
            moveLeft();
        }
        if( Input.GetKey( playerStats.controls.rightKey ) )
        {
            moveRight();
        }
    }

    private void moveForward()
    {
        rigidbody.AddForce(Vector3.forward * playerStats.movementSpeed);
    }

    private void moveBack()
    {
        rigidbody.AddForce(Vector3.back * playerStats.movementSpeed);
    }

    private void moveLeft()
    {
        rigidbody.AddForce(Vector3.left * playerStats.movementSpeed);
    }

    private void moveRight()
    {
        rigidbody.AddForce(Vector3.right * playerStats.movementSpeed);
    }

	// Use this for initialization
	void Start () {
        //Ask for the rigidbody component on the GameObject!
        rigidbody = GetComponent<Rigidbody>();
	}
	
    void FixedUpdate() //MAKE SURE YOU SPELL THIS PROPERLY!!!!
    {
        checkKeyPresses();
    }

	// Update is called once per frame
	void Update () {
	
	}
}
