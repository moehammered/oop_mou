﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Item
{
    public string name;
    public string description;
    public string useMessage;
}

public class PlayerInventory : MonoBehaviour {

    public Item[] items;
    public int currentIndex;

    private void useItem()
    {
        print(items[currentIndex].useMessage);
    }

    private void checkKeys()
    {
        if(Input.GetKeyDown(KeyCode.LeftBracket))
        {
            decreaseIndex();
        }
        if(Input.GetKeyDown(KeyCode.RightBracket))
        {
            increaseIndex();
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            useItem();
        }
    }

    private void increaseIndex()
    {
        currentIndex++;

        if(currentIndex > items.Length-1)
        {
            currentIndex = items.Length - 1;
        }
    }

    private void decreaseIndex()
    {
        currentIndex--;

        if(currentIndex < 0)
        {
            currentIndex = 0;
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        checkKeys();
	}
}
