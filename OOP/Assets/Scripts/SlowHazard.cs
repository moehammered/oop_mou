﻿using UnityEngine;
using System.Collections;

public class SlowHazard : MonoBehaviour {

    public float slowFactor = 3f;

    void OnTriggerEnter(Collider col)
    {
        //Did i collide with something that has a "Player" tag?
        if(col.transform.tag == "Player")
        {
            //Great I did! So try and find a Rigidbody component on this object
            Rigidbody playerBody = col.gameObject.GetComponent<Rigidbody>();
            //did i find a rigidbody?
            if(playerBody != null)
            {
                //Great I did!!! Now add force to push the player backwards!
                playerBody.AddForce(-playerBody.transform.forward * slowFactor, ForceMode.Impulse);
                //My job is done, kill myself
                Destroy(gameObject);
            }
        }
    }
}
