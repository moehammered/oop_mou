﻿using UnityEngine;
using System.Collections;

public class FunctionTutorial : MonoBehaviour {

    public string myName;
    public int myAge;
    public string myGender;
    public string mySentence;

	// Use this for initialization
	void Start () 
    {
        printMyName();
        printID();
	}
	
	// Update is called once per frame
	void Update () {
        checkKeyPresses();
	}

    public void printMyName()
    {
        print("Moe!");
    }

    public void printID()
    {
        print(myName);
        print(myAge);
        print(myGender);
        print(mySentence);
    }

    public void checkKeyPresses()
    {
        //Input.GetKey = Holding the key down
        //Input.GetKeyDown = Pressing the key once
        if(Input.GetKey(KeyCode.W))
        {
            print("W");
            movePlayerForward();
        }
        if(Input.GetKeyDown(KeyCode.A))
        {
            print("A");
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            print("S");
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            print("D");
        }
    }

    public void movePlayerForward()
    {
        print("Player is moving forward");
        transform.Translate(Vector3.forward * Time.deltaTime);
    }
}

