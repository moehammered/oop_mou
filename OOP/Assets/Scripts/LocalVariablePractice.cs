﻿using UnityEngine;
using System.Collections;

public class LocalVariablePractice : MonoBehaviour {

    public string playerName;
    public string sentence;

    private void setPlayerName()
    {
        string tempName;

        tempName = "Player 2";
        playerName = "Player 1";
    }

    private void createSentence()
    {
        string sentence;
        sentence = ""; //assigns an empty string

        if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            sentence += "My name is " + playerName;
        }
        if(Input.GetKeyDown(KeyCode.Alpha2))
        {
            sentence += "\n"; //Makes a new line in the string
            sentence += "One day I died. The end.";
        }
        if(sentence.Length > 0) //is there any letters in the sentence?
        {
            print(sentence);
        }
    }

    private void createDungeonMessage()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            string message;
            message = "You have entered a dungeon.\n";
            sentence += message;
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            string message;
            message = "You see a dragon!\n";
            sentence += message;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            string message;
            message = "You are fucked!\n";
            sentence += message;
        }
        if (Input.GetKeyDown(KeyCode.U))
        {
            string message;
            message = "You are the champion!\n";
            sentence += message;
        }
        if(Input.GetKeyDown(KeyCode.Return))
        {
            print(sentence);
            sentence = "";
        }
    }

	// Use this for initialization
	void Start () {
        setPlayerName();
	}
	
	// Update is called once per frame
	void Update () {
        createSentence();
        createDungeonMessage();
	}
}
