﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisplayPlayerScore : MonoBehaviour {

    public Text scoreText;
    public InfiniteRunner player;

	// Update is called once per frame
	void Update () {
        scoreText.text = "Score: " + player.score;
	}

}
