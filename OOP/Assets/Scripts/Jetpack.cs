﻿using UnityEngine;
using System.Collections;

public class Jetpack : MonoBehaviour {

	public KeyCode key;
	public float thrust;
	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(Input.GetKey(key))
		{
			rb.AddForce(Vector3.up * thrust);
		}
	}
}
