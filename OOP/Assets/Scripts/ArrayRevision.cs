﻿using UnityEngine;
using System.Collections;

public class ArrayRevision : MonoBehaviour {

	public int[] collectiveAges;
	public int arraySize;
    public int currentIndex;
	
    private void increaseIndex()
    {
        currentIndex++; //increment currentIndex by 1

        if( currentIndex > arraySize - 1 ) //Is currentIndex greater than the last element?
        {
            currentIndex = arraySize - 1;
            Debug.LogWarning("Number exceeded last element number! CurrentIndex set to Size - 1");
        }
    }

    private void decreaseIndex()
    {
        currentIndex--; //decrement currentIndex by 1

        if( currentIndex < 0 ) //Is currentIndex less than 0?
        {
            currentIndex = 0;
            Debug.LogWarning( "Negative numbers are out of range! CurrentIndex set to 0." );
        }
    }

    private void setAge()
    {
        collectiveAges[currentIndex] = 20;
    }

    private void checkKeys()
    {
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            print("Up arrow is pressed!");
            //Alternatively: currentIndex += 1;
            //Alternativelyely: currentIndex = currentIndex + 1;
            increaseIndex();
        }
        if( Input.GetKeyDown(KeyCode.DownArrow) )
        {
            print("Down arrow is pressed!");
            decreaseIndex();
        }
        if( Input.GetKeyDown(KeyCode.Return) )
        {
            print("Enter Pressed!");
            setAge();
        }
    }

	private void setInitialValues()
	{
		for( int index = 0; index < collectiveAges.Length; index++ )
		{
			collectiveAges[index] = -1;
		}
	}
	
	// Use this for initialization
	void Start () {
		collectiveAges = new int[arraySize];
		setInitialValues();
	}
	
	// Update is called once per frame
	void Update () {
        checkKeys();
	}
}
