﻿using UnityEngine;
using System.Collections;

public class ScopeModifier : MonoBehaviour {

    public ScopePractice otherClass;
    public string messageCopy;

	// Use this for initialization
	void Start () {
        
        otherClass.scopeFunction();
        otherClass.classNumber = 4;
        otherClass.message = "MMk";
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Return))
        {
            otherClass.printSecretMessage();
        }
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            otherClass.changeSecretMessage("I want!");
        }
        if (Input.GetKeyDown(KeyCode.Backslash))
        {
            messageCopy = otherClass.getSecretMessage();
        }
	}
}
