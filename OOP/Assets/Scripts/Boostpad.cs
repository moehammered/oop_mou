﻿using UnityEngine;
using System.Collections;

public class Boostpad : MonoBehaviour {

    public float forceAmount; //the power of the boost when applied
    private Rigidbody playerBody; //the variable which holds the player's rigidbody

	private void OnTriggerEnter(Collider col)
    {
        if( col.tag == "Player" ) //check the tag of the object we collided
        {
            //get the rigidbody component off of the gameObject we collided with!
            playerBody = col.GetComponent<Rigidbody>();
            //apply a force - 'transform.forward' means the object's forward facing direction
            applyForce(transform.forward, forceAmount);
        }
    }

    private void applyForce(Vector3 direction, float speed)
    {
        playerBody.AddForce(direction * speed, ForceMode.Impulse);
    }
}
