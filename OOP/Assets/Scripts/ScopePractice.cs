﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct StructScope   //Created within file scope
{                           //Marks beginning of struct scope
    public int number;      //created within Type/Struct scope
    public bool boolean;    //created within Type/Struct scope
    public string word;     //created within Type/Struct scope
}                           //Marks end of struct scope

public class ScopePractice : MonoBehaviour {

    public StructScope structVariable; //Created within the class scope of ScopePractice
    public int classNumber; //is visible due to being inside the class scope
    public bool exists;
    public string messageInABottle;
    public bool checkIf;
    public string message;
    private string secretMessage = "Woo spoopie!";

    public string getSecretMessage()
    {
        return secretMessage;
    }

    public void changeSecretMessage(string newMessage)
    {
        if(newMessage.Length > 0 && newMessage.Length < 10)
        {
            secretMessage = newMessage;
        }
        else
        {
            Debug.LogWarning("You tried to change the secretMessage incorrectly!");
        }
    }

    public void printSecretMessage()
    {
        print(secretMessage);
    }

	// Use this for initialization
	void Start () {
        bool exists;
        exists = true;
        this.exists = exists;

        scopeFunction();

        //Modified within the Start Scope, and accesses the struct's scope via the '.' operator
        structVariable.number = 1; 
        structVariable.boolean = true;
        structVariable.word = "string";
	}
	
	// Update is called once per frame
	void Update () {
        string messageInABottle;
        messageInABottle = "Message";

        this.messageInABottle = messageInABottle;
	}

    public void scopeFunction()
    {
        bool checkIf;
        checkIf = true;

        this.checkIf = checkIf;

        if(checkIf)
        {
            string message;
            message = "Inside if statement scope!";

            this.message = message;
        }
    }
}
