﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Profile
{
    public int health;
    public string name;
    public float movementSpeed;
    public float jumpHeight;
    public KeyBinds controls;
}

public class Player_Complete : MonoBehaviour {

    public Profile stats;
    private Rigidbody rigidbody;

    public void moveRight()
    {
        rigidbody.AddForce(Vector3.right * stats.movementSpeed);
    }

    public void moveLeft()
    {
        rigidbody.AddForce(Vector3.left * stats.movementSpeed);
    }

    public void moveBack()
    {
        rigidbody.AddForce(Vector3.back * stats.movementSpeed);
    }

    public void moveForward()
    {
        rigidbody.AddForce(Vector3.forward * stats.movementSpeed);
    }

    public void checkKeypresses()
    {
        if (Input.GetKey(stats.controls.forwardKey))
        {
            moveForward();
        }
        if (Input.GetKey(stats.controls.backKey))
        {
            moveBack();
        }
        if (Input.GetKey(stats.controls.leftKey))
        {
            moveLeft();
        }
        if (Input.GetKey(stats.controls.rightKey))
        {
            moveRight();
        }
        if(Input.GetKeyDown(stats.controls.jumpKey))
        {
            rigidbody.AddForce(Vector3.up * stats.jumpHeight, ForceMode.Impulse);
        }
    }

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate()
    {
        checkKeypresses();
    }
}
