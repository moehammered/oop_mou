﻿using UnityEngine;
using System.Collections;

public class BouncingBall : MonoBehaviour {

	public float bounciness;
	private Rigidbody rb;
	
	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}
	
	void OnCollisionEnter(Collision col)
	{
		rb.AddForce(Vector3.up * bounciness, ForceMode.Impulse);
	}
}
