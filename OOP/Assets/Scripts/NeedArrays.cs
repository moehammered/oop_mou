﻿using UnityEngine;
using System.Collections;

public class NeedArrays : MonoBehaviour {

    public string studentOne;
    public string studentTwo;
    public string studentThree;
    public string studentFour;

	// Use this for initialization
	void Start () {
        studentOne = "Moe";
        studentTwo = "Bob";
        studentThree = "Jo";
        studentFour = "Adopted";
        renameStudents();
	}
	
    public void renameStudents()
    {
        studentOne = "Why?";
        studentTwo = "This is annoying!";
        studentThree = "MoMo";
        studentFour = "JoJo";
    }

	// Update is called once per frame
	void Update () {
	
	}
}
