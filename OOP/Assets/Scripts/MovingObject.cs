﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour {

    public float speed;
    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>(); //get the rigidbody component
	}
	
    private void moveForward()
    {
        rb.AddForce(Vector3.forward * speed);
    }

    private void moveBack()
    {
        rb.AddForce(Vector3.back * speed);
    }

    private void moveLeft()
    {
        rb.AddForce(Vector3.left * speed);
    }

    private void moveRight()
    {
        rb.AddForce(Vector3.right * speed);
    }

    private void move( Vector3 direction, float forceAmount )
    {
        rb.AddForce( direction * forceAmount );
    }

    private void checkKeys()
    {
        if(Input.GetKey(KeyCode.W))
        {
            //moveForward
            //moveForward();
            move(Vector3.forward, speed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            //moveBack
            //moveBack();
            move(Vector3.back, speed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            //moveLeft
            //moveLeft();
            move(Vector3.left, speed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            //moveRight
            //moveRight();
            move(Vector3.right, speed);
        }
        if(Input.GetKey(KeyCode.Space))
        {
            move( Vector3.up, speed*10 );
        }
    }

	// Update is called once per frame
	void FixedUpdate () {
        checkKeys();
	}
}
