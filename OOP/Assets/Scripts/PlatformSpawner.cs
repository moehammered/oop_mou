﻿using UnityEngine;
using System.Collections;

public class PlatformSpawner : MonoBehaviour {

    public GameObject prefab;
    public float offset = 2;

    void OnTriggerEnter(Collider col)
    {
        if(col.transform.tag == "Player")
        {
            //spawn the next platform
            spawnPlatform(offset);
        }
    }

    void spawnPlatform(float offset)
    {
        GameObject spawnedPlatform = (GameObject)Instantiate(prefab);

        spawnedPlatform.transform.position = gameObject.transform.position;

        spawnedPlatform.transform.Translate(transform.forward * offset);
    }
}
