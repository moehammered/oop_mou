﻿using UnityEngine;
using System.Collections;

public class FunctionsRevisited : MonoBehaviour {

    public string myName;
    public int myAge;
    public string myGender;
    public float number1;
    public float number2;
    public float result;

    private void subtractNumbers( float num1, float num2 )
    {
        result = num1 - num2;
        print(num1 - num2);
    }

    private void addNumbers( float num1, float num2 )
    {
        result = num1 + num2;
        print(num1 + num2);
    }

    public void setName(string name)
    {
        myName = name;
    }

    public void setAge(int age)
    {
        myAge = age;
    }

    public void setGender(string gender)
    {
        myGender = gender;
    }

    public void setPersonalDetails( string name, int age, string gender )
    {
        setName(name);
        setAge(age);
        setGender(gender);
    }

    private void checkKeys()
    {
        if( Input.GetKeyDown(KeyCode.KeypadPlus) )
        {
            addNumbers(number1, number2);
        }
        if( Input.GetKeyDown(KeyCode.KeypadMinus) )
        {
            subtractNumbers(number1, number2);
        }
    }

	// Use this for initialization
	void Start () {
        //setName("Dickhead");
        //setAge(28);
        //setGender("Boy");
        setPersonalDetails("Congrats", 100, "No Dick");
        subtractNumbers(5, 6);
        addNumbers(5, 6);
	}
	
	// Update is called once per frame
	void Update () {
        checkKeys();
	}
}
