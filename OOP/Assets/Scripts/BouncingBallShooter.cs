﻿using UnityEngine;
using System.Collections;

public class BouncingBallShooter : MonoBehaviour {

	public GameObject bulletPrefab;
	public float shootingSpeed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Return))
		{
			GameObject spawnedBullet = Instantiate(bulletPrefab) as GameObject;
			spawnedBullet.transform.position = transform.position;
			Rigidbody bulletBody = spawnedBullet.GetComponent<Rigidbody>();
			bulletBody.AddForce(Vector3.forward * shootingSpeed, ForceMode.Impulse);
		}
	}
}
