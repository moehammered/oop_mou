﻿using UnityEngine;
using System.Collections;

[System.Serializable] //Make it visible in the inspector!!
public struct KeyBinds
{
    public KeyCode forwardKey;
    public KeyCode backKey;
    public KeyCode leftKey;
    public KeyCode rightKey;
    public KeyCode jumpKey;
}

public class RigidbodyMover : MonoBehaviour {

    public KeyBinds controls; //A variable with a type of 'KeyBinds'
    public float movementSpeed;

    private Rigidbody rigidbody;

    public void checkKeypresses()
    {
        if(Input.GetKey(controls.forwardKey))
        {
            moveForward();
        }
        if(Input.GetKey(controls.backKey))
        {
            moveBack();
        }
        if(Input.GetKey(controls.leftKey))
        {
            moveLeft();
        }
        if(Input.GetKey(controls.rightKey))
        {
            moveRight();
        }
    }

    public void moveRight()
    {
        rigidbody.AddForce(Vector3.right * movementSpeed);
    }

    public void moveLeft()
    {
        rigidbody.AddForce(Vector3.left * movementSpeed);
    }

    public void moveBack()
    {
        rigidbody.AddForce(Vector3.back * movementSpeed);
    }

    public void moveForward()
    {
        rigidbody.AddForce(Vector3.forward * movementSpeed);
    }

	// Use this for initialization
	void Start () {
        //Get the Rigidbody component off of the GameObject so we can
        //use it in our code here!
        rigidbody = GetComponent<Rigidbody>();
	}
	
    void FixedUpdate()
    {
        checkKeypresses();
    }

	// Update is called once per frame
	void Update () {
	
	}
}
