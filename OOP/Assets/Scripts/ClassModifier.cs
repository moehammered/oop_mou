﻿using UnityEngine;
using System.Collections;

public class ClassModifier : MonoBehaviour {

    public ClassPractice otherClass;

	// Use this for initialization
	void Start () {
        otherClass.printNumber();
        otherClass.printFloat();
        otherClass.printWord();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
