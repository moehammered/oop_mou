﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct LoginInfo
{
    public string loginName;
    public string password;
    public string personalMessage;
    public int age;
}

public class CharacterRegistrator : MonoBehaviour {

    public LoginInfo[] players;

    public void printLoginInfo()
    {
        print(players[0]); //This accesses the entire element
        print(players[0].loginName); //This accesses the loginName from the element
        print(players[0].password); //This accesses the password from the element
        print(players[0].personalMessage); //This accesses the personalMessage from the element
        print(players[0].age); //This accesses the age from the element
    }

	// Use this for initialization
	void Start () {
        printLoginInfo();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
