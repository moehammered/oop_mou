﻿using UnityEngine;
using System.Collections;

//Structs are created here!!!!!!
[System.Serializable]
public struct Address
{
    public int houseNumber;
    public string streetName;
    public string suburb;
    public int postcode;
    public string state;
    public string country;
}

public class StructTutorial : MonoBehaviour {

    //Scope type    name    ;
    public Address myAddress;

    public void setStartingAddress()
    {
        myAddress.houseNumber = 213;
        myAddress.streetName = "Pacific Hwy";
        myAddress.suburb = "St Leonards";
        myAddress.postcode = 2065;
        myAddress.state = "NSW";
        myAddress.country = "Australia";
    }

    private void changeAddress()
    {
        myAddress.houseNumber = 123;
        myAddress.streetName = "Fake Street";
        myAddress.suburb = "Springfield";
        myAddress.postcode = 1000;
        myAddress.state = "????";
        myAddress.country = "Merica";
    }

	// Use this for initialization
	void Start () {
        setStartingAddress();
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Space))
        {
            changeAddress();
        }
	}
}
