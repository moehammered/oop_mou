﻿using UnityEngine;
using System.Collections;

public class InfiniteRunner : MonoBehaviour {

    public int score;
    public float movementSpeed;
    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();	
	}
	
    void Update()
    {
        score++;
    }

	// Update is called once per frame
	void FixedUpdate () {
        //move the player in the forward direction he is facing!!!
        rb.AddForce(transform.forward * movementSpeed);
	}
}
