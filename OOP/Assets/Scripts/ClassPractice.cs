﻿using UnityEngine;
using System.Collections;

public class ClassPractice : MonoBehaviour {

    public int number;
    public float decimalNumber;
    public bool aBoolean;
    public string aWord;
    public string anotherWord;

    public void printNumber()
    {
        print(number);
    }

    public void printFloat()
    {
        print(decimalNumber);
    }

    public void printBool()
    {
        print(aBoolean);
    }

    public void printWord()
    {
        print(aWord);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
